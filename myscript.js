    var GET_DATA = ()=>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
        };
        fetch("https://fm2wdkflw6.execute-api.us-east-2.amazonaws.com/flights2", requestOptions)
        // con.log()
        .then(response => response.text())
        .then(result => {
            res=JSON.parse(result);
            var table = document.getElementById('flights2');
            var arrHead = new Array();
            arrHead = ['id', 'from', 'to', 'departure', 'arrival'];
            var arrValue = new Array();
            for(i=0; i<res.Count; i++){
                data = res.Items[i];
                toPush = [data.id, data.from, data.to, data.departure, data.arrival];
                arrValue.push(toPush);
            }
            var tr = table.insertRow(-1);

            for (var h = 0; h < arrHead.length; h++) {
                var th = document.createElement('th');
                th.innerHTML = arrHead[h];
                tr.appendChild(th);
            }

            for (var c = 0; c <= arrValue.length - 1; c++) {
                tr = table.insertRow(-1);

                for (var j = 0; j < arrHead.length +2; j++) {
                    var td = document.createElement('td');
                    td = tr.insertCell(-1);
                    if (j == arrHead.length)
                        td.innerHTML = "<button id='btnEdit'onclick='editRec(this)'>Edit</button>"
                    else
                        if (j == arrHead.length + 1)
                            td.innerHTML = "<button id='btnDel' onclick='callAPI_DELETE(this)'>Delete</button>";
                    else
                        td.innerHTML = arrValue[c][j];
                }
            }
        }) 
        .catch(error => console.log('error', error));
    }

    var PUT_DATA = (id,from,to,departure,arrival)=>{
        if(id == "" || from == "" || to == "" || departure == "" || arrival == ""){
            alert("Please fill all the fields")
        }
        else{
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            var raw = JSON.stringify({"id":id, "from":from, "to":to, "departure":departure, "arrival":arrival});
            var requestOptions = {
                method: 'PUT',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };
            fetch("https://fm2wdkflw6.execute-api.us-east-2.amazonaws.com/flights2", requestOptions)
            .then(response => response.text())
            .then(result => console.log(JSON.parse(result)))
            .catch(error => console.log('error', error))
            .then(() => {window.location.reload();});
        }
    }

    function editRec(element){
        document.getElementById("addrec").style.visibility="hidden";
        document.getElementById("formEdit").style.visibility="visible";
        rowIndex = element.parentNode.parentNode.rowIndex;
        id = document.getElementsByTagName('table')[0].rows[rowIndex].cells[0].innerText;
        document.getElementById("updateid").value=id;
        from = document.getElementsByTagName('table')[0].rows[rowIndex].cells[1].innerText;
        document.getElementById("updatefrom").value = from;
        to = document.getElementsByTagName('table')[0].rows[rowIndex].cells[2].innerText;
        document.getElementById("updateto").value = to;
        departure = document.getElementsByTagName('table')[0].rows[rowIndex].cells[3].innerText;
        document.getElementById("updatedeparture").value = departure;
        arrival = document.getElementsByTagName('table')[0].rows[rowIndex].cells[4].innerText;
        document.getElementById("updatearrival").value = arrival;
      
    }

    function callAPI_DELETE(element){
        rowIndex = element.parentNode.parentNode.rowIndex;
        id = document.getElementsByTagName('table')[0].rows[rowIndex].cells[0].innerText
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow'
        };
        fetch("https://fm2wdkflw6.execute-api.us-east-2.amazonaws.com/flights2/" + id, requestOptions)
        .then(response => response.text())
        .then(result => console.log(JSON.parse(result)))
        .catch(error => console.log('error', error))
        .then(() => {window.location.reload();});
    }

